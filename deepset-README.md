## German BERT model
It can be converted from TF to PyTorch following https://sites.google.com/deepset.ai/wiki/ml/bert/evaluating-bert?authuser=1#h.p_SVTTbVCXcZtC

## Configuring run_ner.py

You can set the IP of the mlFlow tracking server to visualize the results.

## Run the NER task with:

```
python run_ner.py \
--experiment=ExperimentName \
--run_name=RunName \
--data_dir=data/CoNLL \
--bert_model=pytorch_german_bert \
--task_name=ner \
--output_dir=out_debugging \
--do_train \
--do_eval \
--learning_rate=5e-5 \
--num_train_epochs=10 \
--train_batch_size=64 \
--warmup_proportion=0.4
```

Make sure to set up a proper ***experiment***, a ***run_name***, point the ***bert_model*** to the proper directory (or pass a shortcut name for a pre-trained BERT model:

- `bert-base-uncased`
- `bert-base-cased`
- `bert-base-multilingual-uncased`
- `bert-base-multilingual-cased` <- Recommended

and make sure you have the correct ***data_dir*** selected (`data/CoNLL`, `data/GermEval` or `data/combined`).